extern crate hex;

use num::bigint::BigUint;
use num::FromPrimitive;
use num::Integer;
use num::ToPrimitive;
use sha2::{Digest, Sha512};

#[cfg(test)]
mod tests;

struct Alphabet {
    data: Vec<char>,
    length: BigUint,
}

pub fn generate_password(password: String, service: String) -> String {
    let combined_password = password + &service;

    let alphabet = get_alphabet();
    let mut hash_num = generate_hash_num(combined_password);

    let mut generated_password = String::from("");

    while generated_password.len() < 20 {
        let divmod = hash_num.div_mod_floor(&alphabet.length);

        hash_num = divmod.0;
        let index = divmod.1;

        generated_password.push(alphabet.data[index.to_usize().unwrap()]);
    }

    generated_password
}

fn generate_hash_num(passphrase: String) -> BigUint {
    let mut hasher = Sha512::new();
    hasher.update(passphrase);

    let result = hasher.finalize();

    let hash_hex = hex::encode(result);
    let hash_bytes = hash_hex.as_bytes();

    BigUint::parse_bytes(&hash_bytes, 16).unwrap()
}

fn get_alphabet() -> Alphabet {
    let alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    Alphabet {
        data: alphabet.chars().collect(),
        length: BigUint::from_usize(alphabet.len()).unwrap(),
    }
}
