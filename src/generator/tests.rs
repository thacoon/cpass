use super::*;

#[test]
fn generate_same_passwords_as_cpass() {
    let password = String::from("Super Secret");
    let service = String::from("gitlab2018");

    let actual_password = generate_password(password, service);

    assert_eq!(actual_password, "E03lbLXvaO4optTigYAk");
}

#[test]
fn generate_same_hash_num_as_cpass() {
    let passphrase = String::from("Super Secret");

    let actual_hash_num = generate_hash_num(passphrase);

    let expected_hash_num = "9964445139716730555173630934248536138980342611585138842875956138303390467613983825651604054912126339036058002294118211928084373889538400900814386416695280";
    let expected_hash_num = (expected_hash_num).parse::<BigUint>().unwrap();

    assert_eq!(actual_hash_num, expected_hash_num);
}

#[test]
fn alphabet_stores_correct_length() {
    let actual_alphabet = get_alphabet();

    assert_eq!(
        actual_alphabet.length,
        BigUint::from_usize(actual_alphabet.data.len()).unwrap()
    );
}
