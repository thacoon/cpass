extern crate num;
extern crate sha2;

use std::error::Error;

mod cmdline;
mod generator;

pub fn run() -> Result<(), Box<dyn Error>> {
    cmdline::menu();

    Ok(())
}
