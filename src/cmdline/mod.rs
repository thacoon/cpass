extern crate rpassword;

use std::io;
use std::io::Write;

use super::generator;

pub fn menu() {
    print_help();

    loop {
        print!("> ");
        io::stdout().flush().unwrap();

        let mut choosed_option = String::new();
        io::stdin()
            .read_line(&mut choosed_option)
            .expect("Failed to read user input");

        let choosed_option: u8 = match choosed_option.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Please enter a number.");
                continue;
            }
        };

        match choosed_option {
            0 => show_services(),
            1 => generate_password(),
            2 => generate_new_password(),
            3 => {
                println!("Have a nice day.");
                return;
            }
            _ => {
                println!("Please enter a valid number.");
                continue;
            }
        }
    }
}

fn print_help() {
    println!("0 - Show all services.");
    println!("1 - Generate password.");
    println!("2 - Generate new password.");
    println!("3 - Exit.");
}

fn ask_for_service() -> String {
    let mut service = String::new();

    print!("Enter service: ");
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(&mut service)
        .expect("Failed to read user input");

    let len_without_newline = service.trim_end().len();
    service.truncate(len_without_newline);

    service
}

fn generate_password() {
    let password = ask_for_password();
    let service = ask_for_service();

    let generated_password = generator::generate_password(password, service);
    println!("Finished: {}", generated_password);
}

fn generate_new_password() {
    let password_1 = ask_for_password();
    let password_2 = ask_for_password();

    if password_1 != password_2 {
        println!("Passwords do not match");
        return;
    }

    let service = ask_for_service();

    let generated_password = generator::generate_password(password_1, service);
    println!("Finished: {}", generated_password);
}

fn ask_for_password() -> String {
    rpassword::prompt_password_stdout("Enter master password: ").unwrap()
}

fn show_services() {
    println!("Please stand by. Loading implementation...");
}
