extern crate cpass;

use std::process;

fn main() {
    if let Err(e) = cpass::run() {
        println!("Application error: {}", e);
        process::exit(1);
    }
}
