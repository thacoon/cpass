cpass
=====
[![pipeline status](https://gitlab.com/thacoon/cpass/badges/master/pipeline.svg)](https://gitlab.com/thacoon/cpass/commits/master)

## Usage
```
$ cargo run
0 - Show all services.
1 - Generate password.
2 - Generate new password.
3 - Exit.
> 1
Enter master password:
Enter service: gitlab2018
Finished: MIMAlWCtHJACZebR6im7
> 2
Enter master password:
Enter master password:
Enter service: github2018
Finished: Kk0lPPbzCZuw27pc3xEB
> 3
Have a nice day.
```


## Development

### Developer tools

Usage of developers tools is advised. And is also checked on the CI.

### Code Style

Code needs to follow [community code style](https://github.com/rust-lang/rustfmt).

```bash
# Rustfmt needs to be added with
$ rustup component add rustfmt
# Automatically fix code style with
$ cargo fmt
```

### Compiler Warnings

Code should produce no compiler warnings.

```bash
# Automatically fix warnings with
$ cargo fix
```

### Linters

Code should not have any common mistakes than can be catched with [clippy](https://github.com/rust-lang/rust-clippy).

```bash
# Clippy needs to be installed with
$ rustup component add clippy
# Run Clippy on a Cargo project with
$ cargo clippy --all-targets --all-features --
```
